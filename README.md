# First Round
This is the qualifying round.  The team that ranks in the top ten will proceed to the next round. You can choose to answer any question first.

Details of the Challenges.

- [Web Application and Mobile](https://docs.google.com/spreadsheets/d/1GwAMc87960o6y1dncvsEtG85Qk_uBkMQmWVpdciqpEI/edit#gid=0)

- [And the others categories](https://github.com/wongyos/WCTF23-Qualifying-Round)

## Web Application
Write-ups
| Difficulty | Challenge Name           | Write-ups                          | Special Thanks |
|------------|--------------------------|------------------------------------|----------------|
| Easy       | Account Takeover         | [Web 1 - Account Takeover copy.md](https://github.com/BarwSirati/WTCTT-2023-WriteUp/blob/main/Web%20Application/Web%201%20-%20Account%20Takeover%20copy.md) | [BarwSirati](https://github.com/BarwSirati)     |
| Easy       | SQLi then HMAC           | [Web 2 - SQLi then HMAC](https://github.com/BarwSirati/WTCTT-2023-WriteUp/blob/main/Web%20Application/Web%202%20-%20SQLi%20then%20HMAC.md)                   | [BarwSirati](https://github.com/BarwSirati)     |
| Medium     | Path Traversal over vHost | [Web 3 - Path Traversal over vHost](https://github.com/BarwSirati/WTCTT-2023-WriteUp/blob/main/Web%20Application/Web%203%20-%20Path%20Traversal%20over%20vHost.md) | [BarwSirati](https://github.com/BarwSirati)     |
| Hard       | White Box Code Analysis  | -                                  | -              |


And The additional write-ups

[WTCTT 2023 รอบคัดเลือก : Web Challenges By Ar3mus](https://ar3mus.hashnode.dev/wtctt-2023-web-challenges?fbclid=IwAR2IfeftjZntA8xkIHRTN389apEtOcQXvYltJv9r4rGmfwwGgcDnhJ79IOE_aem_AfOI_PR5AeWckrTee4lGL8CH9O3b7Q2mJGunAUBj_-Zg73vS8-ILULknucbzeLvlyTs)


## Digital Forensic
## Reverse Engineering & Pwnable
## Network Security
## Mobile Security
Write-ups
| Difficulty | Challenge Name               | Write-ups                                        | Special Thanks                                                             |
|------------|------------------------------|--------------------------------------------------|----------------------------------------------------------------------------|
| Easy       | Hidden Message in APK         | [Mobile 1 - Hidden Message in APK](https://github.com/BarwSirati/WTCTT-2023-WriteUp/blob/main/Mobile%20Security/Mobile%201%20-%20Hidden%20Message%20in%20APK.md)         | [BarwSirati](https://github.com/BarwSirati/WTCTT-2023-WriteUp/commits?author=BarwSirati)               |
| Easy       | TOTP Token Generator          | [Mobile 2 - TOTP Token Generator](https://github.com/BarwSirati/WTCTT-2023-WriteUp/blob/main/Mobile%20Security/Mobile%202%20-%20TOTP%20Token%20Generator.md)            | [BarwSirati](https://github.com/BarwSirati/WTCTT-2023-WriteUp/commits?author=BarwSirati)               |
| Medium     | Broken Firebase AuthN         | [Mobile 3 - Broken Firebase AuthN](https://github.com/BarwSirati/WTCTT-2023-WriteUp/blob/main/Mobile%20Security/Mobile%203%20-%20Broken%20Firebase%20AuthN.md)           | [BarwSirati](https://github.com/BarwSirati/WTCTT-2023-WriteUp/commits?author=BarwSirati)               |
| Hard       | The Winner of SecGuy 888      | [Mobile 4 - The Winner of SecGuy 888](https://github.com/BarwSirati/WTCTT-2023-WriteUp/blob/main/Mobile%20Security/Mobile%204%20-%20The%20Winner%20of%20SecGuy%20888.md)  | [BarwSirati](https://github.com/BarwSirati/WTCTT-2023-WriteUp/commits?author=BarwSirati)               |


## Programming
## Cryptography

# Final Round
OMG, This is an offline round. I'm very excited to visit the city from the countryside. Since it's located in Siam Paragon. While competitive many tourists watch me -v-, I'm so shy -///-. And the rules are same as the first round.

Details of the Challenges.

- [All categories neither web nor mobile.](https://github.com/wongyos/WCTF23-Final-Round)

## Web Application
## Digital Forensic
## Reverse Engineering & Pwnable
## Network Security
## Mobile Security
## Programming
## Cryptography