# Programming - Medium : Lotto Locked

Hi, I'm nam-ob ><. It's my first time writing the write-ups. Since I wish to do the write-ups while practicing my English, kindly accept my apologies if anything or grammar is incorrect. And please feel free to share any suggestions or comments you might have. Your feedback is valuable for me.

# Challenge Preview

This challenge provide the description below.

>ฉันว่าหวยมันล็อค เธอเห็นด้วยกับฉันไหม (I think the lotto has been predetermined. Do you agree?)

>nc 202.29.103.199 10001

>หมายเหตุ รูปแบบของ Flag ที่เป็นคำตอบของข้อนี้คือ WCTF23{md5}


and it also given the zip file that contains the code below.

```import random
money = 0
print('Welcome To WCTF23 Lotto')
print('You Will Have 1000 Times To Win The Flag')
for i in range(1, 1001):
    print('----------------------------------------')
    print('Try Your Lucky [Times:' + str(i) + ']')
    digit1 = random.randint(61, 70) % 3 + 1
    digit2 = random.randint(61, 70) % 9 + 1
    lotto = str(digit1) + str(digit2)
    guess = input('Your 2 Digit Lotto: ')
    print('System 2 Digit Lotto:', lotto)
    if guess == lotto:
        print('You Won Lotto')
        money += 1000
    else:
        print('You Loss Lotto')
    print('Your Money:', money)
    if money >= 85000:
        print('WCTF23{md5}')
        break
```

# My suggestion

After read the code, I saw
- first digits of lotto can be 1,2,3
- second digist can be 1,2,3,4,5,6,7,8,9
When I knew this, I can increase percent to win the lotto.

When I use 
`nc 202.29.103.199 10001` 
I got this.

![when nc](./resource/when_nc.jpg)

Let's try to suggest some numbers to win the lotto, what do you think? what number will achive this?
I chose number 22.

![try 22](./resource/Screenshot%202023-12-30%20230911.jpg)

# Solution

Before my hands are broken. I come up with an idea to bruteforce all possible numbers by writing code.

I wrote this code with my friend names ChatGPT. Many thanks to you <3
remark I'm too lazy to exclude numbers 20 and 30

```
import socket
import logging

# Create and configure the logger
logging.basicConfig(filename='lotto.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

def send_input(sock, message):
    sock.sendall(message.encode() + b'\n')

# Server details
host = '202.29.103.199'
port = 10001

for i in range(11,40) :
    # Connect to the server
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))

        while True:
            data = s.recv(1024).decode()  # Receive data from the server
            print(data, end='')  # logging.info the received data

            if "Your 2 Digit Lotto:" in data:
                send_input(s, str(i))  # Send '22' as input to the server

            if "Times:1000" in data:
                print(data, end='')
                logging.info(data + '\n')  # logging.info the received data
                logging.info(str(i) + '\n')
                break

        if "WCTF23" in data:
            logging.info(str(i) + '\n')
            break
```

Yeahhhh after run some test it's working, so let's execute it!!! (I'm stuck in this process too long due to the network problem T^T.)

When it finished running I still don't got the flag. So I investigate logs, then I see the magic number is 28.

![log](./resource/image-1.png)


After keep running the magic number. Finally! I won the lottery (Hope this happens in my real life too ><)

![flag](./resource/image.png)