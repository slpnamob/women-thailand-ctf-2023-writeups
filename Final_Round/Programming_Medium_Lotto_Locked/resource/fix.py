import socket
import logging

# Create and configure the logger
logging.basicConfig(filename='lotto.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

def send_input(sock, message):
    sock.sendall(message.encode() + b'\n')

# Server details
host = '202.29.103.199'
port = 10001

for i in range(28,29) :
    # Connect to the server
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))

        while True:
            data = s.recv(1024).decode()  # Receive data from the server
            print(data, end='')  # logging.info the received data

            if "Your 2 Digit Lotto:" in data:
                send_input(s, str(i))  # Send '22' as input to the server

            if "Times:1000" in data:
                print(data, end='')
                logging.info(data + '\n')  # logging.info the received data
                logging.info(str(i) + '\n')
                break

        if "WCTF23" in data:
            logging.info(str(i) + '\n')
            break
            